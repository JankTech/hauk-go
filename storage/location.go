package storage

import "time"

type Location struct {
	Lat      float64   `json:"lat"`
	Lon      float64   `json:"lon"`
	Time     time.Time `json:"time"`
	SID      string    `json:"sid"`
	Speed    float64   `json:"spd,omitempty"`
	Accuracy float64   `json:"acc,omitempty"`
	Provider bool      `json:"prv,omitempty"`
}
