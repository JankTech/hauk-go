package memory

import (
	"errors"
	"time"

	"github.com/google/uuid"
	"gitlab.com/janktech/hauk-go/storage"
)

var ErrNotFound = errors.New("not found")

type store struct {
	sessions map[string]session
}
type Session interface {
	GetPoints(sinceTime time.Time) (map[time.Time]storage.Location, error)
	AddPoint(l storage.Location) error
}
type session struct {
	points map[time.Time]storage.Location
}

func NewStore() store {
	return store{}
}

func (s *store) NewSession() (uuid.UUID, error) {
	u, err := uuid.NewRandom()
	if err != nil {
		return uuid.Nil, err
	}
	s.sessions[u.String()] = session{}
	return u, nil
}

func (s *store) GetSession(SID uuid.UUID) (Session, error) {
	sesh, ok := s.sessions[SID.String()]
	if !ok {
		return &session{}, ErrNotFound
	}
	return &sesh, nil
}

func (s *session) GetPoints(sinceTime time.Time) (map[time.Time]storage.Location, error) {
	filtered := map[time.Time]storage.Location{}
	for t, p := range s.points {
		if t.After(sinceTime) {
			filtered[t] = p
		}
	}

	return filtered, nil
}

func (s *session) AddPoint(l storage.Location) error {
	s.points[l.Time] = l
	return nil
}
