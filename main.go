package main

import (
	"encoding/json"
	"fmt"
	"log"
	"log/slog"
	"net/http"
	// "time"

	"github.com/google/uuid"
	"gitlab.com/janktech/hauk-go/storage"
	"gitlab.com/janktech/hauk-go/storage/memory"
)

type sessionRepository interface {
	NewSession() (uuid.UUID, error)
	GetSession(SID uuid.UUID) (memory.Session, error)
}

func main() {
	store := memory.NewStore()
	http.HandleFunc("/", handleRoot(&store))
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func handleRoot(s sessionRepository) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodPost {
			handleUpdateLocation(s, w, r)
			return
		}
		http.NotFound(w, r)
	}
}

func handleUpdateLocation(s sessionRepository, w http.ResponseWriter, r *http.Request) {
	b := storage.Location{}
	d := json.NewDecoder(r.Body)
	d.Decode(&b)

	u, err := uuid.Parse(b.SID)
	if err != nil {
		var sessionErr error
		u, sessionErr = s.NewSession()
		if sessionErr != nil {
			slog.Error("Error creating session", "error", err.Error())
			return
		}
	}

	repo, err := s.GetSession(u)
	if err != nil {
		slog.Error("Error getting session", "error", err.Error())
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}
	err = validateLocationData(b)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = repo.AddPoint(b)
	if err != nil {
		slog.Error("Error adding point", "error", err.Error())
		http.Error(w, "Internal server error", http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(http.StatusCreated)
}

func validateLocationData(b storage.Location) error {

	if b.Lat < -90 || b.Lat > 90 {
		return fmt.Errorf("expected lat value between -90 and 90, got %f", b.Lat)
	}

	if b.Lon < -180 || b.Lon > 180 {
		return fmt.Errorf("expected lon value between -180 and 180, got %f", b.Lon)
	}

	return nil
}
