# Hauk GO: A Revitalized Real-time Location Sharing Solution

## 🚀 Introduction

Hauk GO is an ongoing project aimed at reinventing Hauk, shifting its foundations from an interpreted to a compiled language - Go, ensuring enhanced performance, maintainability, and robustness.

Currently this project is named after the original repository, [hauk](https://github.com/bilde2910/Hauk), since it is aiming to replicate its functionality. If it ends up deviating significantly enough to be no longer compatible with the original, I will seek to rename it.
## 🏗️ Why Hauk GO?

* __Revitalized Development:__ The original Hauk project has been inactive since 2021, necessitating a resurgence to continue its evolution.
* __Enhanced Maintainability:__ While functional, the original codebase presented challenges for upkeep. Hauk GO offers a cleaner, more manageable implementation.
* __Optimized Performance:__ Transitioning from an interpreted to a compiled language eliminates the inherent sluggishness, providing a high-speed alternative.

## 🛠️ Installation

```sh
go get -u gitlab.com/janktech/hauk-go
```

## 🚀 Usage

To run Hauk GO, execute the following command:

```sh
go run hauk-go
```

## 📚 Documentation

TODO:

## 🤝 Contributing

Contributions, issues, and feature requests are welcome! Feel free to check issues page if you want to contribute.
## 📃 License

Hauk GO is under the MIT license. For more information, please refer to the LICENSE file in our repository.
## 📈 Future Plans

* Implement API in GO
* Recreate the app frontend in React Native
* Build front end web map
* Deploy the app to the Google Play Store and Apple App Store

## 🙏 Acknowledgments
* Special thanks to [bilde2910](https://github.com/bilde2910) for the original Hauk project.

## 🤖 Developers

[Luke Manson](https://gitlab.com/lmansonhttps://gitlab.com/)

For any additional information or queries, please reach out to the developers.

Happy Coding! 🎉This is a work in progress.
